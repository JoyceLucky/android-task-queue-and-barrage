package com.example.lineuppoject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    LineUpTaskHelp lineUpTaskHelp;

    int index = 0;

    private EditText et_time;
    YearBarrageView barrageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // 文本框。
        et_time = findViewById(R.id.et_time);
        barrageView = findViewById(R.id.barrageView);
        lineUpTaskHelp = LineUpTaskHelp.getInstance();

        findViewById(R.id.btn_on).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String timeOut = et_time.getText().toString().trim();
                if (TextUtils.isEmpty(timeOut)) return;

                //发送弹幕
                List<BarrageBean> barrageBeans = new ArrayList<>();
                BarrageBean barrageBean = new BarrageBean();
                barrageBean.setUserName("我是弹幕" + (index++));
                barrageBeans.add(barrageBean);
                barrageView.setData(barrageBeans);
                barrageView.start();


                ConsumptionTask task = new ConsumptionTask();
                task.taskNo = "Task" + (index++); // 确保唯一性
                task.planNo = "No9527"; // 将数据分组， 如果没有该需求的同学，可以不进行设置
                // 开始执行任务之前，设置超时时间，
                task.timeOut = Long.parseLong(timeOut) * 1000; //
                lineUpTaskHelp.addTask(task); // 如果还有任务没完成，添加到排队列表中去。
                //et_time.setText(""); // 清空


            }
        });


        findViewById(R.id.btn_tasks).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConsumptionTask task4 = new ConsumptionTask();
                task4.taskNo = "Task" + (index++); // 任务一定要确保唯一性
                task4.planNo = "No9528";
                task4.timeOut = (60 * 1000) * 1;
                /**
                 *  插入一个任务至列队中；
                 *  第二个参数，是需要插入列队位置，从0开始；
                 *  在插队时要注意列队的任务数量，如果插队的位置大于列队的长度是不会成功的。
                 */
                lineUpTaskHelp.insertTask(task4, 0);
            }
        });


        findViewById(R.id.btn_runn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestRunnable runnable = new RequestRunnable();
                runnable.taskNo = "Task" + (index++); // 任务一定要确保唯一性
                runnable.planNo = "No9528";
                runnable.timeOut = 1000 * 10;
                runnable.runnable = new ExTaskThread().setTask(runnable);
                lineUpTaskHelp.addTask(runnable);

            }
        });

        initListener();
    }


    /**
     * 注册任务监听
     */
    private void initListener() {
        lineUpTaskHelp.setOnTaskListener(new LineUpTaskHelp.OnTaskListener() {
            @Override
            public void exNextTask(ConsumptionTask task) {

                if (task instanceof RequestRunnable) {
                    Log.e("Post", "RequestRunnable");
                    RequestRunnable requestRunnable = (RequestRunnable) task;
                    requestRunnable.runnable.start();
                } else if (task instanceof ConsumptionTask) {
                    Log.e("Post", "ConsumptionTask");
                }
            }

            @Override
            public void noTask() {
                Log.e("Post", "所有任务执行完成");
            }

            @Override
            public void timeOut(ConsumptionTask task) {
                // 超时了.
                Log.e("Post", "超时了,任务ID为：" + task.taskNo);
                // 检查列队， 发现任务并开始执行
                lineUpTaskHelp.exOk(task);
            }
        });
    }


    public class ExTaskThread extends Thread {

        private ConsumptionTask task;

        public ExTaskThread setTask(ConsumptionTask task) {
            this.task = task;
            return this;
        }

        public void run() {
            Log.e("Post", "开始执行任务" + task.taskNo + "设置的超时时间为：" + (task.timeOut / 1000) + "s");
            SystemClock.sleep(2 * 1000); // 模拟任务执行过程
            //发送弹幕
            List<BarrageBean> barrageBeans = new ArrayList<>();
            BarrageBean barrageBean = new BarrageBean();
            barrageBean.setUserName("我是弹幕：" + (index++));
            barrageBeans.add(barrageBean);
            barrageView.setData(barrageBeans);
            barrageView.start();

            task.isResult = true; // 执行完成，标记为已经执行(不管任务执行成功或者失败都要在任务执行完成之后赋值为true)
            if (task.isTimeOut) {
                // 。。 如果是超时引起的错误，将回调timeOut方法
                return;
            }
            Log.e("Post", "任务执行完成--------任务ID为:" + task.taskNo);
//                if(isOdd(System.currentTimeMillis())){
//                    // 模拟任务执行的结果====失败，如果一个任务失败了会导致整个计划失败，请调用此方法。
//                    Log.e("Post","任务失败了，结束掉相关联正在排队的任务组");
//                    lineUpTaskHelp.deletePlanNoAll(task.planNo);
//                }

            lineUpTaskHelp.exOk(task);
        }


    }


    public boolean isOdd(long i) {
        return (i & 1) == 1;
    }
}
